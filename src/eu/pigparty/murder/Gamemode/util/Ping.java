package eu.pigparty.murder.Gamemode.util;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import eu.pigparty.murder.Gamemode.game.GameCore;

public class Ping implements Listener {
	@EventHandler
	public void onServerListPing(ServerListPingEvent event) {
		event.setMotd("[" + GameCore.getGamestate() + "]");
	}

}
