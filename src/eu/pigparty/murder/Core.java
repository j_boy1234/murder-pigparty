package eu.pigparty.murder;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import eu.pigparty.murder.Gamemode.game.GameCore;
import eu.pigparty.murder.config.ConfigUtil;

public class Core extends JavaPlugin{

	Core plugin = this;

	public void onEnable() {
		
		plugin = this;
		if(ConfigUtil.getInstance().getBoolean("enabled", plugin) == true){
		load();
		}else{
			Bukkit.getLogger().severe("Murder is set to disabled in the config, disabling");
			Bukkit.getPluginManager().disablePlugin(this);
		}
	}
	
	public void onDisable() {
		plugin = null;
	}
	
	public void load(){
		ConfigUtil.getInstance().setupConfig(plugin);
		GameCore.gamestate = ConfigUtil.getInstance().get("gamestate.lobby", plugin);
	}
}
