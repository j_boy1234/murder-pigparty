package eu.pigparty.murder.config;

import java.io.File;

import org.bukkit.plugin.Plugin;


public class ConfigUtil {
	private static ConfigUtil instance = new ConfigUtil();
	private static Plugin p;

	private ConfigUtil() {

	}

	public static ConfigUtil getInstance() {
		return instance;
	}
	

	public String get(String option, Plugin pl){
		ConfigUtil.p = pl;
		return pl.getConfig().getString(option);
	}
	
	public Boolean getBoolean(String boo, Plugin pl){
		ConfigUtil.p = pl;
		if(pl.getConfig().getBoolean(boo) == true){
			return true;
		}else{
			return false;
		}
	}
	
	public void set(String option, Object value, Plugin pl){
		ConfigUtil.p = pl;
		p.getConfig().set(option, value);
	}
	
	public void setupConfig(Plugin pl){
		ConfigUtil.p = pl;

		try {
			if (!p.getDataFolder().exists()) {
				p.getDataFolder().mkdirs();
			}
			File file = new File(p.getDataFolder(), "config.yml");
			File chat = new File(p.getDataFolder(), "chat.yml");
			if(!chat.exists()){
				chat.createNewFile();
				
			}
			if (!file.exists()) {
				p.saveDefaultConfig();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
